//
//  FlickrMainCharactersViewController.swift
//  MarvelAssignment
//
//  Created by Mahmoud Fathelbab on 1/20/18.
//  Copyright © 2018 Mahmoud Fathelbab. All rights reserved.
//

import UIKit

class FlickrMainCharactersViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let flickrSearchPresenter = MarvelGetCharactersPresenter(serviceManager:ServiceManager())
    
    fileprivate var photos = [FlickrViewData]()
    var searchText = ""
    var currentPage = 1
    var totalPages  = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        flickrSearchPresenter.attachView(self)
        self.title = searchText
        self.performSearchWith(searchText)
        self.initFooterView()
        self.tableView.tableFooterView?.isHidden = false
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Service Call
    
    // request photos from presenter
    func performSearchWith(_ searchText: String)
    {
        flickrSearchPresenter.getFlickerImagesWith(searchTag: searchText, page: currentPage)
        
    }
    
    @IBAction func cancellButtonPressed(_ sender: UIButton)
    {
        self.performSegue(withIdentifier: "showCancel", sender: self)
    }
    func initFooterView()
    {
        let footerView = UIView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: self.tableView.frame.size.width, height: 70))
        footerView.backgroundColor = UIColor.black
        
      //  add activity indicator to tableview
        let activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .white)
        activityIndicator.tag = 10
        activityIndicator.frame = CGRect.init(x: 0, y: 0, width: 20.0, height: 20.0)
        activityIndicator.center = CGPoint.init(x: self.tableView.frame.size.width / 2 + 20, y: 35)
        activityIndicator.startAnimating()
        footerView.addSubview(activityIndicator)
        
        //add footerview to tableview
        self.tableView.tableFooterView = footerView
    }
   
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// MARK: - presenter callback delegate

extension FlickrMainCharactersViewController: MarvelGetCharactersViewProtocol {
    
    func startLoading()
    {
        
        DispatchQueue.main.async {
            
        }
    }
    func finishLoading()
    {
        DispatchQueue.main.async {
            
        }
        
    }
    func setFlickrData(_ images: [FlickrViewData])
    {
        self.photos.append(contentsOf: images)
        DispatchQueue.main.async {
            if self.photos.count == 0
            {
            }
            else
            {
                self.tableView.reloadData()
                
            }
        }
    }
    func failedToGetFlickerData()
    {
        
    }
    
}
// MARK: - UICollectionViewDataSource

extension FlickrMainCharactersViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.photos.count

    }
    
    
   
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MarvelTableViewCellId", for: indexPath) as! MarvelTableViewCell
        
        let photoModel = self.photos[indexPath.row]
        if let imageUrl = URL.init(string: photoModel.fullImageUrl){
            cell.marvelImageView?.sd_setImage(with: imageUrl, placeholderImage: UIImage.init(named: "image_not_available"))
            
        }
        cell.marvelLabel.text = photoModel.name
        return cell
    }
    
    
 

}
// MARK: - UICollectionViewDelegate

extension FlickrMainCharactersViewController: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 177.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.photos.count > 0
        {
            if indexPath.row == self.photos.count - 1
            {
                currentPage += 10
                performSearchWith(searchText)
            }
            
        }
    }
   
    
}








