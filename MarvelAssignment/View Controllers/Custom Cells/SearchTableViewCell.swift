//
//  SearchTableViewCell.swift
//  MarvelAssignment
//
//  Created by Mahmoud Fathelbab on 1/21/18.
//  Copyright © 2018 Mahmoud Fathelbab. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

   
    @IBOutlet weak var marvelImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
