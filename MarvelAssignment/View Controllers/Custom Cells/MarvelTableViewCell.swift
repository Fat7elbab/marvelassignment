//
//  MarvelTableViewCell.swift
//  MarvelAssignment
//
//  Created by Mahmoud Fathelbab on 1/20/18.
//  Copyright © 2018 Mahmoud Fathelbab. All rights reserved.
//

import UIKit

class MarvelTableViewCell: UITableViewCell {

    @IBOutlet weak var marvelImageView: UIImageView!
    
    @IBOutlet weak var marvelLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
