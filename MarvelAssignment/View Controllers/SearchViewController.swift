//
//  SearchViewController.swift
//  MarvelAssignment
//
//  Created by Mahmoud Fathelbab on 1/21/18.
//  Copyright © 2018 Mahmoud Fathelbab. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let flickrSearchPresenter = MarvelGetCharactersPresenter(serviceManager:ServiceManager())
    
    fileprivate var photos = [FlickrViewData]()
    var searchText = ""
    var currentPage = 1
    var totalPages  = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        let cancelButtonAttributes: NSDictionary = [NSAttributedStringKey.foregroundColor: UIColor.red]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes as? [NSAttributedStringKey : AnyObject], for: UIControlState.normal)
        flickrSearchPresenter.attachView(self as MarvelGetCharactersViewProtocol)
        self.title = searchText
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Service Call
    
    // request photos from presenter
    func performSearchWith(_ searchText: String)
    {
        flickrSearchPresenter.getFlickerImagesWith(searchTag: searchText, page: currentPage)
        
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// MARK: - presenter callback delegate

extension SearchViewController: MarvelGetCharactersViewProtocol {
    
    func startLoading()
    {
        
        DispatchQueue.main.async {
            
        }
    }
    func finishLoading()
    {
        DispatchQueue.main.async {
            
        }
        
    }
    func setFlickrData(_ images: [FlickrViewData])
    {
        self.photos = images
        DispatchQueue.main.async {
            if self.photos.count == 0
            {
            }
            else
            {
                self.tableView.reloadData()
                
            }
        }
    }
    func failedToGetFlickerData()
    {
        
    }
    
}
// MARK: - UICollectionViewDataSource

extension SearchViewController: UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.photos.count
        
    }
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCellId", for: indexPath) as! SearchTableViewCell
        
        let photoModel = self.photos[indexPath.row]
        if let imageUrl = URL.init(string: photoModel.fullImageUrl){
            cell.marvelImageView?.sd_setImage(with: imageUrl, placeholderImage: UIImage.init(named: "image_not_available"))
            
        }
        cell.titleLabel.text = photoModel.name
        return cell
    }
    
    
    
    
}
// MARK: - UICollectionViewDelegate

extension SearchViewController: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
   
    
    
}


extension SearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""
        {
            self.photos = []
            self.tableView.reloadData()
        }
        else
        {
            self.performSearchWith(searchText)

        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationController?.popViewController(animated: true)
    }
    
}














