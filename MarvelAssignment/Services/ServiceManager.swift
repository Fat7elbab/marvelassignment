//
//  ServiceManager.swift
//  MarvelAssignment
//
//  Created by Mahmoud Fathelbab on 1/20/18.
//  Copyright © 2018 Mahmoud Fathelbab. All rights reserved.
//

import UIKit

class ServiceManager {
    
    typealias FlickrApiResponse = (NSError?, Int, [MarvelCharctersModel]?) -> Void
    
    // mobiquity apikey
    
    struct FlickrApi {
        static let privateKey = "d3166d885c7b4cf35079af9282c9970a7fa93f6f"
        static let publicKey = "3a8ff496976dd2c437ea8be23957b4df"
        static let searchMarvelFormat = "https://gateway.marvel.com:443/v1/public/characters?ts=%@&apikey=%@&hash=%@&offset=%i&limit=10"
        
    }
    
    // page size for pagination
    
    struct PageSize {
        static let count = 10
    }
    
    struct Errors {
        static let invalidAccessErrorCode = 409
    }
    
    func fetchPhotosForSearchText(searchText: String, pageNumber: Int,  onCompletion: @escaping FlickrApiResponse) -> Void {
        let date : String = String(NSDate().timeIntervalSince1970)
        var md5String = date + FlickrApi.privateKey + FlickrApi.publicKey
        md5String = md5String.md5()
        var format = FlickrApi.searchMarvelFormat
        var args: [CVarArg] = []
        if searchText != ""
        {
            format = format + "&nameStartsWith=%@"
            args = [date, FlickrApi.publicKey, md5String , pageNumber ,searchText]

        }
        else
        {
            args = [date, FlickrApi.publicKey, md5String , pageNumber]

        }
        let urlString = String(format: format, arguments: args)
        let url: NSURL = NSURL(string: urlString)!
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        let searchTask = URLSession.shared.dataTask(with: url as URL, completionHandler: {data, response, error -> Void in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
            if error != nil {
                onCompletion(error as NSError?, 0, nil)
                return
            }
            
            do {
                let resultsDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: AnyObject]
                guard let results = resultsDictionary else { return }
                
                if let statusCode = results["code"] as? Int {
                    if statusCode == Errors.invalidAccessErrorCode {
                        let invalidAccessError = NSError(domain: "com.flickr.api", code: statusCode, userInfo: nil)
                        onCompletion(invalidAccessError, 0, nil)
                        return
                    }
                }
                
                guard let photosContainer = resultsDictionary!["data"] as? NSDictionary else { return }
                
                // get maped array of photos related to term
                
                guard let photosArray = photosContainer["results"] as? [NSDictionary] else { return }
                
                let flickrPhotos: [MarvelCharctersModel] = photosArray.map { photoDictionary in
                    
                    let name = photoDictionary["name"] as? String ?? ""
                    let description = photoDictionary["description"] as? String ?? ""
                    let thumbnailDictionary = photoDictionary["thumbnail"] as? NSDictionary ?? [:]
                    let flickrPhoto = MarvelCharctersModel(name: name, description: description, thumbnailDictionary: thumbnailDictionary)
                    return flickrPhoto
                }
                
                onCompletion(nil, 20, flickrPhotos)
                
            } catch let error as NSError {
                print("Error parsing JSON: \(error)")
                onCompletion(error, 0, nil)
                return
            }
            
        })
        searchTask.resume()
    }
    
}

