//
//  MarvelCharctersModel.swift
//  MarvelAssignment
//
//  Created by Mahmoud Fathelbab on 1/20/18.
//  Copyright © 2018 Mahmoud Fathelbab. All rights reserved.
//

import UIKit
import SwiftyJSON
struct MarvelCharctersModel
{

    let name         :  String
    let description  :  String
    let thumbnailDictionary  :  NSDictionary

    var fullPhotoUrl :NSURL{
        
        return getImageUrl()
        
    }
    
    
    // get full image url of flickr photo
    
    private func getImageUrl() -> NSURL {
        return NSURL(string:  JSON(thumbnailDictionary)["path"].string! + "." + JSON(thumbnailDictionary)["extension"].string!)!
    }



}
