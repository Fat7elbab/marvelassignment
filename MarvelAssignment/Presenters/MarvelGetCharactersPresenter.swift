//
//  MarvelGetCharactersPresenter.swift
//  MarvelAssignment
//
//  Created by Mahmoud Fathelbab on 1/20/18.
//  Copyright © 2018 Mahmoud Fathelbab. All rights reserved.
//

import UIKit

struct FlickrViewData{
    let fullImageUrl: String
    let name: String
}

protocol MarvelGetCharactersViewProtocol: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setFlickrData(_ images: [FlickrViewData])
    func failedToGetFlickerData()
}

class MarvelGetCharactersPresenter {
    weak fileprivate var flickerSearchView : MarvelGetCharactersViewProtocol?
    
    fileprivate let serviceManager:ServiceManager
    
    init(serviceManager:ServiceManager){
        self.serviceManager = serviceManager
    }
    
    
    func attachView(_ view:MarvelGetCharactersViewProtocol){
        flickerSearchView = view
    }
    
    func detachView() {
        flickerSearchView = nil
    }
    
    func getFlickerImagesWith(searchTag: String , page: Int){
        self.flickerSearchView?.startLoading()
        serviceManager.fetchPhotosForSearchText(searchText: searchTag, pageNumber: page) { (error, totalImagesCount, flickerPhotos) in
            
            self.flickerSearchView?.finishLoading()
            if error == nil
            {
                if let photos = flickerPhotos{
                    let mappedImages = photos.map{
                        return FlickrViewData(fullImageUrl: "\($0.fullPhotoUrl)" , name: "\($0.name)" )
                    }
                    
                    self.flickerSearchView?.setFlickrData(mappedImages)
                }
            }
            else
            {
                self.flickerSearchView?.failedToGetFlickerData()
                
            }
            
        }
        
    }
}

