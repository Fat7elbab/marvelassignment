//
//  FlickrSearchPresenterTest.swift
//  FlickrAssignment
//
//  Created by Mahmoud Fathelbab on 7/23/17.
//  Copyright © 2017 Mobiquity. All rights reserved.
//

import XCTest
@testable import MarvelAssignment


class ServiceManagerMock: ServiceManager {
    fileprivate let photos: [MarvelCharctersModel]!
    fileprivate let error: Error?
    fileprivate let total: Int

    init(photos: [MarvelCharctersModel] , error: Error? , total :Int) {
        self.photos = photos
        self.error = error
        self.total = total
    }
    override func fetchPhotosForSearchText(searchText: String, pageNumber: Int,  onCompletion: @escaping FlickrApiResponse) -> Void {
        onCompletion(self.error as NSError?, self.total , self.photos)
    }
    
}

class FlickerSearchViewProtocolMock :NSObject ,  MarvelGetCharactersViewProtocol
{
    var setPhotosCalled = false
    var setErrorCalled = false

    
    
    func startLoading()
    {
    
    }
    func finishLoading()
    {
    
    }
    func setFlickrData(_ images: [FlickrViewData])
    {
        self.setPhotosCalled = true
    
    }
    func failedToGetFlickerData()
    {
        self.setErrorCalled = true

    }
    
}

class MarvelSearchPresenterTest: XCTestCase {
    
    
    
    let twoPhotos = ServiceManagerMock(photos: [MarvelCharctersModel(name: "mahmoud", description: "A good chef never tells his secrets.", thumbnailDictionary: ["path":"http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available" ,"extension" : "jpg"]), MarvelCharctersModel(name: "mahmoud", description: "A good chef never tells his secrets.", thumbnailDictionary: ["path":"http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available" ,"extension" : "jpg"])] , error: nil , total :2)
    
    
    let withInvalidFlickrKey = ServiceManagerMock(photos: [MarvelCharctersModel]() , error: NSError(domain: "com.flickr.api", code: 409, userInfo: nil) , total :0)
    

    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    
   
    func testShouldSetPhotos() {
        //given
        let flickerSearchViewMock = FlickerSearchViewProtocolMock()
        let flickerSearchPresenterUnderTest = MarvelGetCharactersPresenter(serviceManager: twoPhotos)
        flickerSearchPresenterUnderTest.attachView(flickerSearchViewMock)
        
        //when
        flickerSearchPresenterUnderTest.getFlickerImagesWith(searchTag: "pets", page: 1)
        
        //verify
        XCTAssertTrue(flickerSearchViewMock.setPhotosCalled)
    }
    
    func testShouldFailedInValidApiKey() {
        //given
        let flickerSearchViewMock = FlickerSearchViewProtocolMock()
        let flickerSearchPresenterUnderTest = MarvelGetCharactersPresenter(serviceManager: withInvalidFlickrKey)
        flickerSearchPresenterUnderTest.attachView(flickerSearchViewMock)
        
        //when
        flickerSearchPresenterUnderTest.getFlickerImagesWith(searchTag: "pets", page: 1)
        
        //verify
        XCTAssertTrue(flickerSearchViewMock.setErrorCalled)
    }

}

